package app.controller;

import app.model.DataSourceInfo;
import app.model.DataSourceJoinInfo;
import app.model.SparkResult;
import app.service.SparkService;
import com.github.ywilkof.sparkrestclient.FailedSparkRequestException;
import com.github.ywilkof.sparkrestclient.JobStatusResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "api/v2/spark")
public class SparkController {

    @Autowired
    private SparkService sparkService;

    private Logger logger = LogManager.getLogger(SparkController.class);

    /**
     * Retrieves submission status (details) for specified submission ID.
     *
     * @param id submission ID
     * @return {@link JobStatusResponse} containing submission details
     * @throws FailedSparkRequestException if request fails
     */
    @GetMapping(path="/jobs/status/{id}", produces = "application/json")
    public JobStatusResponse getSubmissionStatus(@PathVariable String id) throws FailedSparkRequestException {
        logger.info("Retrieving job status, id: {}", id);
        return sparkService.getJobStatus(id);
    }

    /**
     * Submits a spark job to create a new data source for specified {@link DataSourceInfo}.
     * Supports 2 creation modes defined by "generateDs" flag:
     * 1. generate data - generates DS data of a given size
     * 2. from CSV file - retrieves DS data from SCV file at specified location (with or without header)
     *
     * @param info data source info
     * @return {@link ResponseEntity} with {@link SparkResult} containing submission details
     */
    @PostMapping(path="/jobs/ds/creator", consumes = "application/json", produces = "application/json")
    public ResponseEntity createDataSource(@Validated @RequestBody DataSourceInfo info) {
        logger.info("Submitting DS creator spark job: {}", info.toString());
        SparkResult result = sparkService.create(info);
        if (!result.isSuccess())
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(result);
    }

    /**
     * Submits a spark job to create a new data source for specified {@link DataSourceJoinInfo}.
     * Defaults to "INNER" join if join type is not specified.
     * Generates joined data source name if joinedDsName is not provided.
     *
     * @param joinInfo data source join info
     * @return {@link ResponseEntity} with {@link SparkResult} containing submission details
     */
    @PostMapping(path="/jobs/ds/joiner", consumes = "application/json", produces = "application/json")
    public ResponseEntity joinDataSources(@Validated @RequestBody DataSourceJoinInfo joinInfo) {
        SparkResult result = sparkService.join(joinInfo);
        if (!result.isSuccess())
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(result);

    }
}
