package app.model;

import org.springframework.hateoas.Link;

public class SparkResult {
    private boolean success;
    private String message;
    private String submissionId;
    private String status;
    private Link statusLink;
    private Object entity;

    public boolean isSuccess() {
        return success;
    }

    public SparkResult setSuccess(boolean success) {
        this.success = success;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public SparkResult setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getSubmissionId() {
        return submissionId;
    }

    public SparkResult setSubmissionId(String submissionId) {
        this.submissionId = submissionId;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public SparkResult setStatus(String status) {
        this.status = status;
        return this;
    }

    public Link getStatusLink() {
        return statusLink;
    }

    public SparkResult setStatusLink(Link statusLink) {
        this.statusLink = statusLink;
        return this;
    }

    public Object getEntity() {
        return entity;
    }

    public SparkResult setEntity(Object entity) {
        this.entity = entity;
        return this;
    }

    @Override
    public String toString() {
        return "SparkResult{" +
                "success=" + success +
                ", message='" + message + '\'' +
                ", submissionId='" + submissionId + '\'' +
                ", status='" + status + '\'' +
                ", statusLink=" + statusLink.toString() +
                ", entity=" + entity.toString() +
                '}';
    }
}
