package app.model;

import app.enums.JoinType;

import javax.validation.constraints.NotBlank;

public class DataSourceJoinInfo {

    @NotBlank(message = "leftDsName name must not be blank!")
    private String leftDsName;
    @NotBlank(message = "rightDsName name must not be blank!")
    private String rightDsName;
    @NotBlank(message = "leftColumnName name must not be blank!")
    private String leftColumnName;
    @NotBlank(message = "leftColumnName name must not be blank!")
    private String rightColumnName;

    private String joinedDsName;
    private JoinType joinType;

    public String getLeftDsName() {
        return leftDsName;
    }

    public void setLeftDsName(String leftDsName) {
        this.leftDsName = leftDsName;
    }

    public String getRightDsName() {
        return rightDsName;
    }

    public void setRightDsName(String rightDsName) {
        this.rightDsName = rightDsName;
    }

    public String getLeftColumnName() {
        return leftColumnName;
    }

    public void setLeftColumnName(String leftColumnName) {
        this.leftColumnName = leftColumnName;
    }

    public String getRightColumnName() {
        return rightColumnName;
    }

    public void setRightColumnName(String rightColumnName) {
        this.rightColumnName = rightColumnName;
    }

    public String getJoinedDsName() {
        return joinedDsName;
    }

    public void setJoinedDsName(String joinedDsName) {
        this.joinedDsName = joinedDsName;
    }

    public JoinType getJoinType() {
        return joinType;
    }

    public void setJoinType(JoinType joinType) {
        this.joinType = joinType;
    }

    @Override
    public String toString() {
        return "DataSourceJoinInfo {" +
                "leftDsName='" + leftDsName + '\'' +
                ", rightDsName='" + rightDsName + '\'' +
                ", leftColumnName='" + leftColumnName + '\'' +
                ", rightColumnName='" + rightColumnName + '\'' +
                ", joinedDsName='" + joinedDsName + '\'' +
                ", joinType=" + joinType +
                '}';
    }
}
