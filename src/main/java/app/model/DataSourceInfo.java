package app.model;

import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.nio.file.*;

public class DataSourceInfo {

    private boolean generateDs;
    @NotBlank(message = "DS name must not be blank!")
    private String name;
    private String csvPath;
    private boolean header;
    @Min(value = 0, message = "Size must not be negative")
    private int size;


    public boolean isGenerateDs() {
        return generateDs;
    }

    public DataSourceInfo setGenerateDs(boolean generateDs) {
        this.generateDs = generateDs;
        return this;
    }

    public String getName() {
        return name;
    }

    public DataSourceInfo setName(String name) {
        this.name = name;
        return this;
    }

    public String getCsvPath() {
        return csvPath == null ? "" : csvPath;
    }

    public DataSourceInfo setCsvPath(String csvPath) {
        if (!isGenerateDs() && StringUtils.isBlank(csvPath))
            throw new IllegalArgumentException("CSV path cannot be empty.");

        this.csvPath = csvPath == null ? "" : csvPath;
        return this;
    }

    public boolean isHeader() {
        return header;
    }

    public DataSourceInfo setHeader(boolean header) {
        this.header = header;
        return this;
    }

    public int getSize() {
        return size;
    }

    public DataSourceInfo setSize(int size) {
        this.size = size;
        return this;
    }

    @Override
    public String toString() {
        return "DataSourceInfo {" +
                "generateDs=" + generateDs +
                ", name='" + name + '\'' +
                ", csvPath='" + csvPath + '\'' +
                ", header=" + header +
                ", size=" + size +
                '}';
    }
}
