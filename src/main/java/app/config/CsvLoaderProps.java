package app.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.validation.constraints.NotBlank;

@Configuration
@ConfigurationProperties("csv")
@PropertySource("classpath:csv-loader.properties")
public class CsvLoaderProps {
    @NotBlank
    private String appName;
    @NotBlank
    private String className;
    @NotBlank
    private String jarLocation;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getJarLocation() {
        return jarLocation;
    }

    public void setJarLocation(String jarLocation) {
        this.jarLocation = jarLocation;
    }

    @Override
    public String toString() {
        return "CsvLoaderProps{" +
                "appName='" + appName + '\'' +
                ", className='" + className + '\'' +
                ", jarLocation='" + jarLocation + '\'' +
                '}';
    }
}
