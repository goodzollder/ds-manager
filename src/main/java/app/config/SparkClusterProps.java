package app.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.validation.constraints.NotBlank;

@Configuration
@ConfigurationProperties("spark")
@PropertySource("classpath:spark-cluster.properties")
public class SparkClusterProps {

    @NotBlank
    private String httpScheme;
    @NotBlank
    private String host;
    @NotBlank
    private Integer port;
    @NotBlank
    private String version;
    @NotBlank
    private String esHost;
    @NotBlank
    private String esPort;

    public String getHttpScheme() {
        return httpScheme;
    }

    public SparkClusterProps setHttpScheme(String httpScheme) {
        this.httpScheme = httpScheme;
        return this;
    }

    public String getHost() {
        return host;
    }

    public SparkClusterProps setHost(String host) {
        this.host = host;
        return this;
    }

    public Integer getPort() {
        return port;
    }

    public SparkClusterProps setPort(Integer port) {
        this.port = port;
        return this;
    }

    public String getVersion() {
        return version;
    }

    public SparkClusterProps setVersion(String version) {
        this.version = version;
        return this;
    }

    public String getEsHost() {
        return esHost;
    }

    public SparkClusterProps setEsHost(String esHost) {
        this.esHost = esHost;
        return this;
    }

    public String getEsPort() {
        return esPort;
    }

    public SparkClusterProps setEsPort(String esPort) {
        this.esPort = esPort;
        return this;
    }

    @Override
    public String toString() {
        return "SparkClusterProps {" +
                "host='" + host + '\'' +
                ", port=" + port +
                ", version='" + version + '\'' +
                ", esHost='" + esHost + '\'' +
                ", esPort='" + esPort + '\'' +
                '}';
    }
}
