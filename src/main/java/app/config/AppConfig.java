package app.config;

import com.github.ywilkof.sparkrestclient.ClusterMode;
import com.github.ywilkof.sparkrestclient.SparkRestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Autowired
    private SparkClusterProps sparkProps;
    /**
     * @see <a href="SparkRestClient">https://github.com/ywilkof/spark-jobs-rest-client</a>
     * @return configured {@link SparkRestClient} instance
     */
    @Bean
    public SparkRestClient restClient() {
        return SparkRestClient.builder()
                              .httpScheme(sparkProps.getHttpScheme())
                              .masterHost(sparkProps.getHost())
                              .masterPort(sparkProps.getPort())
                              .clusterMode(ClusterMode.spark)
                              .sparkVersion(sparkProps.getVersion())
                              .poolingHttpClient(5)
                              .build();
    }
}
