package app.enums;

public enum JoinType {
    INNER("inner"),
    LEFT_OUTER("leftouter");

    private String name;

    JoinType(String value) {
        this.name = value;
    }

    public String value() {
        return this.name;
    }

    @Override
    public String toString() {
        return name;
    }
}
