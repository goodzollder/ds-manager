package app;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DsManagerApp {

    private static final Logger logger = LogManager.getLogger(DsManagerApp.class);

    public static void main(String[] args) {
        logger.info("Starting DS Manager application.");
        SpringApplication.run(DsManagerApp.class, args);
    }
}
