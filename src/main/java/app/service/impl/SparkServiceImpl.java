package app.service.impl;

import app.config.CsvLoaderProps;
import app.config.DsJoinerProps;
import app.config.SparkClusterProps;
import app.controller.SparkController;
import app.enums.JoinType;
import app.model.DataSourceInfo;
import app.model.DataSourceJoinInfo;
import app.model.SparkResult;
import app.service.SparkService;
import com.github.ywilkof.sparkrestclient.FailedSparkRequestException;
import com.github.ywilkof.sparkrestclient.JobStatusResponse;
import com.github.ywilkof.sparkrestclient.SparkRestClient;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Service
public class SparkServiceImpl implements SparkService {

    private static final Logger logger = LogManager.getLogger(SparkServiceImpl.class);

    @Autowired
    private SparkClusterProps sparkProps;

    @Autowired
    private CsvLoaderProps csvLoaderProps;

    @Autowired
    private DsJoinerProps dsJoinerProps;

    @Autowired
    private SparkRestClient client;

    @Override
    public JobStatusResponse getJobStatus(String submissionId) throws FailedSparkRequestException {
        logger.debug("Retrieving submission [{}] status.", submissionId);
        try {
            return client.checkJobStatus()
                         .withSubmissionIdFullResponse(submissionId);
        } catch (FailedSparkRequestException ex) {
            logger.debug("Error retrieving submission [{}] status." +
                                 "\n\tError message: {}" +
                                 "\n\tDriver state: {}",
                         submissionId, ex.getMessage(), ex.getState());
            throw ex;
        }
    }

    @Override
    public SparkResult create(DataSourceInfo dsInfo) {
        logger.debug("Submitting a job with ds info: {}", dsInfo.toString());

        List<String> args = dsInfo.isGenerateDs() ?
                Arrays.asList(dsInfo.getName(), "", "false", String.valueOf(dsInfo.getSize())) :
                Arrays.asList(dsInfo.getName(), dsInfo.getCsvPath(), String.valueOf(dsInfo.isHeader()));
        return submit(csvLoaderProps.getAppName(),
                      csvLoaderProps.getClassName(),
                      csvLoaderProps.getJarLocation(),
                      args, dsInfo);
    }

    @Override
    public SparkResult join(DataSourceJoinInfo dsJoinInfo) {
        // default join type: inner
        if (dsJoinInfo.getJoinType() == null) {
            dsJoinInfo.setJoinType(JoinType.INNER);
        }
        if (StringUtils.isBlank(dsJoinInfo.getJoinedDsName())) {
            dsJoinInfo.setJoinedDsName(String.format("%s_%s_%s-%s-%s",
                                                     dsJoinInfo.getJoinType().value(),
                                                     dsJoinInfo.getLeftDsName(),
                                                     dsJoinInfo.getRightDsName(),
                                                     dsJoinInfo.getLeftColumnName(),
                                                     dsJoinInfo.getRightColumnName()));
        }
        logger.debug("Join parameters: {}", dsJoinInfo.toString());
        List<String> args = Arrays.asList(dsJoinInfo.getLeftDsName(),
                                          dsJoinInfo.getRightDsName(),
                                          dsJoinInfo.getLeftColumnName(),
                                          dsJoinInfo.getRightColumnName(),
                                          dsJoinInfo.getJoinedDsName(),
                                          dsJoinInfo.getJoinType().value());
        return submit(dsJoinerProps.getAppName(),
                      dsJoinerProps.getClassName(),
                      dsJoinerProps.getJarLocation(),
                      args, dsJoinInfo);
    }

    private SparkResult submit(String appName, String className, String jarLocation, List<String> args, Object entity) {
        try {
            String submissionId = client.prepareJobSubmit()
                                        .appName(appName)
                                        .mainClass(className)
                                        .appResource(jarLocation)
                                        .appArgs(args)
                                        .withProperties()
                                        .put("spark.es.nodes", sparkProps.getEsHost())
                                        .put("spark.es.port", sparkProps.getEsPort())
                                        .submit();
            logger.debug("Driver successfully submitted with submission ID: {}", submissionId);

            Link link = linkTo(methodOn(SparkController.class).getSubmissionStatus(submissionId))
                    .withRel("submission status");
            return new SparkResult().setSuccess(true)
                                    .setMessage("Submission succeeded with submission ID:" + submissionId)
                                    .setStatus("submitted")
                                    .setSubmissionId(submissionId)
                                    .setStatusLink(link)
                                    .setEntity(entity);
        } catch (FailedSparkRequestException ex) {
            logger.debug("Failed to submit the job, error message: {}", ex.getMessage());
            return new SparkResult().setSuccess(false)
                                    .setMessage("Submission failed with error:" + ex.getMessage())
                                    .setStatus("failed")
                                    .setEntity(entity);
        }
    }
}
