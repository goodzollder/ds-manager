package app.service;

import app.model.DataSourceInfo;
import app.model.DataSourceJoinInfo;
import app.model.SparkResult;
import com.github.ywilkof.sparkrestclient.FailedSparkRequestException;
import com.github.ywilkof.sparkrestclient.JobStatusResponse;

public interface SparkService {

    /**
     * Retrieves the status of the submission with specified ID.
     *
     * @param submissionId - job submission ID
     * @return {@link JobStatusResponse} containing submission details
     */
    JobStatusResponse getJobStatus(String submissionId) throws FailedSparkRequestException;

    /**
     * Creates a data source for specified {@link DataSourceInfo}.
     *
     * @param dsInfo Data Source info
     * @return job submission request status
     */
    SparkResult create(DataSourceInfo dsInfo);

    /**
     * Joins data sources according to specified {@link DataSourceJoinInfo}.
     *
     * @param dsJoinInfo data source join details
     * @return job submission request status
     */
    SparkResult join(DataSourceJoinInfo dsJoinInfo);
}
