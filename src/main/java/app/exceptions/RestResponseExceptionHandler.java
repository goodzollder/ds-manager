package app.exceptions;

import com.github.ywilkof.sparkrestclient.FailedSparkRequestException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
@RestController
public class RestResponseExceptionHandler extends ResponseEntityExceptionHandler {

    private Logger logger = LogManager.getLogger(RestResponseExceptionHandler.class);

    /**
     * Handles {@link FailedSparkRequestException}.
     * @param ex exception to handle
     * @return response entity with HTTP status
     */
    @ExceptionHandler(FailedSparkRequestException.class)
    public ResponseEntity<ErrorResponse> handleSparkRequestException(FailedSparkRequestException ex) {
        String details = ex.getSuppressed().length > 0 ? ex.getSuppressed()[0].getMessage() : ex.toString();
        String status = ex.getState() == null ? "failed" : ex.getState().name();
        ErrorResponse error = new ErrorResponse(ex.getMessage(), details, status);

        logger.error("FailedSparkRequestException occurred. {}", error.toString());
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Handles {@link IllegalArgumentException}.
     * @param ex exception to handle
     * @return response entity with HTTP status
     */
    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ErrorResponse> handleIllegalArgumentException(IllegalArgumentException ex) {
        logger.error("IllegalArgumentException occurred.");
        String details = ex.getSuppressed().length > 0 ? ex.getSuppressed()[0].getMessage() : ex.toString();
        return new ResponseEntity<>(new ErrorResponse(ex.getMessage(),
                                                      details,
                                                      HttpStatus.BAD_REQUEST.name()),
                                    HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        ErrorResponse response = new ErrorResponse("Field validation error(s).",
                                                   ex.getMessage(),
                                                   HttpStatus.BAD_REQUEST.name());
        List<String> validationList = ex.getBindingResult().getFieldErrors().stream()
                                        .map(DefaultMessageSourceResolvable::getDefaultMessage)
                                        .collect(Collectors.toList());
        logger.info("Validation error list : {}", validationList);
        response.setErrors(validationList);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
