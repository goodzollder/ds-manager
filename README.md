# DS-MANAGER
Spark job manager.  
This project is a part of the data source joiner application bundle.  
See [DS-BUILDER](https://bitbucket.org/goodzollder/ds-builder/src/master/) project for details.  

### Description
DS-MANAGER is a stand-alone Spring-Boot web application that exposes a simple RESTfull API to  
create new and join selected data sources.  
Data source operations are accomplished by submitting pre-compiled spark jobs with desired parameters  
to a Spark cluster via Spark's "hidden" REST API.  

### Installation
The simplest way to start using the application is to run it in a bundle.  
See [DS-BUILDER](https://bitbucket.org/goodzollder/ds-builder/src/master/) project for details.  
However, it can also be run in a stand-alone mode from IDE (IntelliJ), "fat" jar, or in a docker container.

#### Stand-alone mode

##### Prerequisites

* Running Spark cluster.
* Running Elasticsearch instance.
* [csv-loader](https://bitbucket.org/goodzollder/csv-loader/src/master/) 
and [ds-joiner](https://bitbucket.org/goodzollder/ds-joiner/src/master/) jars available at some location.

##### Configuration

Modify properties in [src/main/resources/](src/main/resources/) to reflect your own settings.  
Update [csv-loader.properties](src/main/resources/csv-loader.properties)  
```yml
    csv.jarLocation=/opt/jars/csv-loader-1.0-SNAPSHOT.jar
```    
Update [ds-joiner.properties](src/main/resources/ds-joiner.properties)  
```yml
    joiner.jarLocation=/opt/jars/ds-joiner-1.0-SNAPSHOT.jar
```  
Update [spark-cluster.properties](src/main/resources/spark-cluster.properties)  
```yml
    spark.http-scheme=http
    spark.host=<localhost, domain, or IP>
    spark.port=6066
    spark.version=2.1.0
    spark.es-host=<localhost, domain, or IP>
    spark.es-port=9200
```  
##### Build and run

From the project's root:

- Generate jar with dependencies and run:  
```bash
    ./gradlew clean build
    java -jar build/libs/<jar-name>
```  
- Or, build a docker container and run:  
```bash
    ./gradlew clean build
    docker build -t ds-manager .
    docker run ds-manager:latest -p 8082:8082
```  

DS-MANAGER API will be available at: http://hostname:8082  

### REST API usage and examples

#### Create/define data sources

Create/define data source from generated data:  
```bash
    # generateDs:true - enables data generator in the "creator" job
    curl -XPOST -H "Content-Type: application/json" 'localhost:8082/api/v2/spark/jobs/ds/creator' -d '{
        "generateDs": true,
        "name": "ds1",
        "size": 1000
    }'
```  

Create/define data source from CSV (with or without header):  
```bash
    # header:true - if CSV header is present, false otherwise
    # generateDs:false - retrieve data from CSV
    curl -XPOST -H "Content-Type: application/json" 'localhost:8082/api/v2/spark/jobs/ds/creator' -d '{
        "generateDs": false,
        "name": "ds2",
        "csvPath":"/opt/data/ds2_data.csv",
        "header": true                          
    }'
```  
Data generator:  

- creates 3 columns with headers generated as <ds-name>+_col1/_col2/_col3
- generates unique integer values (IDs) in the first column (as strings)
- generates random 5-character-long alphabetic strings for other column fields
  
Response example:  

```json
{
    "success": true,
    "message": "Submission succeeded with submission ID:driver-20181121162149-0000",
    "submissionId": "driver-20181121162149-0000",
    "status": "submitted",
    "statusLink": {
        "rel": "submission status",
        "href": "http://localhost:8082/api/v2/spark/jobs/status/driver-20181121162149-0000"
    },
    "entity": {
        "generateDs": true,
        "name": "ds1",
        "csvPath": "",
        "header": false,
        "size": 100000
    }
}
```  

#### Join data sources

* Supported join types: _INNER_ (default), _LEFT_OUTER_
* Mandatory fields: _leftDsName_, _rightDsName_, _leftColumnName_, _rightColumnName_
* If missing, _joinedDsName_ is generated: concatenated join type and mandatory field values separated by "_"

```bash
    curl -XPOST -H "Content-Type: application/json" 'localhost:8082/api/v2/spark/jobs/ds/joiner' -d '{
            "leftDsName": "ds1",
            "rightDsName": "ds2",
            "leftColumnName": "ds1_col_1",
            "rightColumnName": "ds2_col_1",
            "joinedDsName": "inner_ds1_ds2",
            "joinType": "INNER"
        }'
```  

#### Get job submission status

Requires job submission ID included in the path.  
Job submission ID is included in the response to _create_ or _join_ requests.  
It also contains the generated "submission status" link (see response sample above).  
```bash
    # example: localhost:8082/api/v2/spark/jobs/status/{submissionId}
    curl http://localhost:8082/api/v2/spark/jobs/status/driver-20181121162149-0000
```



