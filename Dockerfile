FROM openjdk:8-jdk-alpine

MAINTAINER goodzollder@gmail.com

RUN mkdir -pv /tmp/logs
VOLUME /tmp/logs
EXPOSE 8082

# Add application jsr to the container:
ARG JAR_FILE=build/libs/ds-manager-app-0.1.0.jar
ADD ${JAR_FILE} ds-manager.jar

# run jar file
ENTRYPOINT ["java","-jar","/ds-manager.jar"]
